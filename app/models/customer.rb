class Customer < ActiveRecord::Base
  max_paginates_per 20
  has_many :sales

  def total_sales
    self.sales.map(&:amount_with_tax).sum
  end

end
